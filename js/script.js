//JSON

var dicionario = [
        {"termo":"ABR","descricao":"A ABR (available bit rate – taxa de bit disponível) é a quantidade média de dados transferidos por unidade de tempo. Por exemplo: um arquivo que possui uma taxa de bit disponível de 256kbit/s transfere, em média, 256.000 bits a cada segundo."},
        {"termo":"ADVANCED ANALYTICS","descricao":"O Gartner define plataformas de Advanced Analytics como capazes de proporcionar um ambiente end-to-end para desenvolver e implementar modelos. Estas plataformas devem incluir: (1) acesso aos dados a partir de múltiplas fontes; (2) preparação de dados, exploração, e visualização; (3) a capacidade de implantar modelos e integrá-los nos processos de negócios e aplicações; (4) capacidades performáticas da plataforma, do projeto e do modelo de gestão; e de (5) alta escalabilidade de desempenho tanto para o desenvolvimento e implantação."},
        {"termo":"ANSI","descricao":"American national standard institute – instituto nacional americano de padrões."},
        {"termo":"APACHE TOMCAT","descricao":"O Tomcat é um servidor web Java, mais especificamente, um container de servlets."},
        {"termo":"APP","descricao":"A sigla pode ter vários significados, porém no universo computacional, APP é a abreviação de application ou aplicativo. Trata-se de um programa desenvolvido para executar uma função específica, basicamente para o usuário."},
        {"termo":"ARDUÍNO","descricao":"Arduíno é uma plataforma de prototipagem eletrônica de hardware livre e de placa única, projetada com um microcontrolador com suporte de entrada/saída embutido, uma linguagem de programação padrão, a qual tem origem em wiring (plataforma de prototipagem eletrônica de hardware livre), e é essencialmente C/C++. O objetivo do projeto é criar ferramentas que são acessíveis, com baixo custo, flexíveis e fáceis de se usar por artistas e amadores. Principalmente para aqueles que não teriam alcance aos controladores mais sofisticados e de ferramentas mais complicadas."},
        {"termo":"ARPANET","descricao":"A ARPAnet (advanced research projects agency network) é a primeira rede de computadores à base de comutação de pacotes. Criada em 1969 pelo departamento de defesa dos EUA para interligar bases militares e departamentos de pesquisa do governo, deu origem à internet."},
        {"termo":"ARRAY","descricao":"O array é um grupo de elementos com atributos semelhantes (como memória ou disco) que podem ser endereçados individualmente."},
        {"termo":"ASCII","descricao":"O ASCII (american standard code for information interchange – código americano padronizado de intercâmbio de informações) é o conjunto internacional de caracteres com valores decimais que o computador pode produzir. Por exemplo: TAB=9, espaço=32, Letra A=65, letra a=97, número 0=48."},
        {"termo":"AWS","descricao":"A Amazon Web Services (AWS) é uma plataforma de serviços em nuvem segura, oferecendo poder computacional, armazenamento de banco de dados, distribuição de conteúdo e outras funcionalidades para ajudar as empresas em seu dimensionamento e crescimento."},
        {"termo":"B2B","descricao":"B2B (business to business) é a expressão utilizada para indicar operações entre empresas. A natureza dessa operação pode ser revenda, transformação ou consumo."},
        {"termo":"B2C","descricao":"O B2C (business to customer) é a expressão utilizada para indicar operações entre empresa e consumidor final."},
        {"termo":"BACKBONE","descricao":"O backbone é a rede principal por onde circulam todos os dados de todos os clientes da internet."},
        {"termo":"BACKUP","descricao":"O backup é a cópia de dados de um dispositivo de armazenamento a outro para que possam ser restaurados em caso da perda dos dados originais, o que pode envolver apagamentos acidentais ou corrupção de dados."},
        {"termo":"BAM","descricao":"O BAM (business activity management – monitoramento das atividades de negócio) é o termo que define como podemos fornecer acesso em tempo real aos indicadores críticos de desempenho de negócios para aumentar a velocidade e a eficácia das operações de negócios. Ao contrário de monitoramento em tempo real tradicional, o BAM vai buscar a informação em múltiplos sistemas de aplicação e também em outras fontes internas e externas, permitindo uma visão mais ampla e mais rica das atividades empresariais. Saiba mais sobre BAM acessando nosso post sobre o assunto, abaixo:"},
        {"termo":"BBS","descricao":"O BBS (bulletin board system) é um ambiente virtual formado por computadores, modems e um software de comunicação ligados a uma ou mais linhas telefônicas. A partir de uma configuração básica feita com scripts e arquivos de texto, é possível criar seu próprio sistema e conectá-lo a outros por meio de uma rede telefônica usando um modem."},
        {"termo":"BDE","descricao":"O BDE (borland database engine) é um mecanismo de acesso a banco de dados desenvolvido pela Borland, amplamente testado por ser usado no mundo inteiro."},
        {"termo":"BIG DATA","descricao":"Big Data é um termo amplamente utilizado na atualidade para nomear conjuntos de dados muito grandes ou complexos, que os aplicativos de processamento de dados tradicionais ainda não conseguem lidar. Os desafios desta área incluem: análise, captura, curadoria de dados, pesquisa, compartilhamento, armazenamento, transferência, visualização e informações sobre privacidade dos dados."},
        {"termo":"BIOS","descricao":"O BIOS (basic input output system) é o sistema básico de entrada e saída."},
        {"termo":"BI","descricao":"o Business Intelligence (BI) é um software para visualização de indicadores de negócios. Possibilita o tratamento de dados complexos para criar visões simplificadas."},
        {"termo":"BIT","descricao":"O BIT é a sigla da união das palavras BInary digiT ou dígito binário. Trata-se da menor unidade de medida de dados que pode ser armazenada ou transmitida no universo computacional. Um bit tem um único valor, sendo zero ou um, ou seja, verdadeiro ou falso."},
        {"termo":"BOTNET","descricao":"Um botnet é uma coleção de programas conectados à internet que comunicam-se com outros programas similares, a fim de executar tarefas. Também é uma coleção de agentes de software ou bots que executam autonomamente e automaticamente. O termo é geralmente associado com o uso de software malicioso, mas também pode se referir a uma rede de computadores, utilizando software de computação distribuída."},
        {"termo":"BPM","descricao":"O BPM (business process management – gerenciamento de processos de negócio) é uma gestão de processos de negócio. Essa gestão é feita através de uma visão sistêmica e geral da organização e é baseada na definição de parâmetros, monitoramento, controle e adaptação para melhoria contínua dos processos. A automação dos processos também é um objetivo do uso do BPM. Para saber mais sobre BPM acesse o artigo em nosso blog clicando no link abaixo:"},
        {"termo":"BPMN","descricao":"O business process model notation ou notação de modelagem de processos de negócio é um registro desenvolvido para definir e gerenciar processos na esfera de negócios. A principal funcionalidade é facilitar a o entendimento do BPM. Acesse nosso post sobre BPMN para saber mais."},
        {"termo":"BPS","descricao":"O BPS (bits per second – bits por segundo) é a medida de transmissão ou recepção de bits."},
        {"termo":"BYOD","descricao":"O BYOD (bring your own device – traga seu próprio dispositivo) é a possibilidade de utilizar dispositivos pessoais como smartphones, tablets ou notebooks no local de trabalho para uso e conectividade com a rede corporativa. O BYOD proporcionou um aumento na produtividade nas empresas, pois os colaboradores podem utilizar seus próprios equipamentos, tornando-os mais independentes e dispostos a trabalhar."},
        {"termo":"CACHE","descricao":"O cache é um dispositivo de acesso rápido, interno a um sistema, que serve de intermediário entre um operador de um processo e o dispositivo de armazenamento ao qual esse operador acede."},
        {"termo":"CAPEX e OPEX","descricao":"O CAPEX (capital expenditure – despesas de capital) designa o montante de dinheiro despendido na aquisição (ou introdução de melhorias) de bens de capital de uma determinada empresa.O OPEX (operational expenditure) é o capital utilizado para manter ou melhorar os bens físicos de uma empresa, tais como equipamentos, propriedades e imóveis. As despesas operacionais são os preços contínuos para dirigir um produto, o negócio, ou o sistema."},
        {"termo":"CATÁLOGO DE SERVIÇOS","descricao":"O Catálogo de Serviço traz uma visão clara de quais serviços a TI oferece e como a TI agrega valor para os recursos financeiros alocados. Oferece um método para requisitar ou pedir os serviços publicados, viabilizando a boa governança em que os principais termos, condições e controles definidos nele estejam integrados aos processos de prestação de serviço da organização. Também permite que a organização melhore o planejamento, a entrega e o suporte aos serviços, enquanto avalia de forma correta os custos e preços do serviço."},
        {"termo":"CEM","descricao":"CEM (customer experience management – gerenciamento da experiência do usuário) é a percepção da interação entre uma empresa e um consumidor através da visão do próprio cliente. Pode se dizer que o CEM é uma experiência do consumidor através de interações com a marca durante o processo de compra."},
        {"termo":"CI ou IC","descricao":"Configuration item ou item de configuração é um dispositivo que pode ser configurado com o objetivo de se entregar a um serviço de TI. Por exemplo: servidor, roteador, switch, access point, entre outros."},
        {"termo":"CIA ou AIC","descricao":"Os configuration item attribute ou atributos de item de configuração são métricas e indicadores monitorados dentro dos CIs ou ICs. Por exemplo: no monitoramento de um servidor (IC) temos alguns indicadores (AIC) que podem ser monitorados como: CPU, memória, disco, entre outros."},
        {"termo":"CLOCK RATE","descricao":"O clock rate indica a frequência com que o processador de um computador funciona. Seu sistema de medidas é em Hertz (MHz ou GHz, por exemplo)."},
        {"termo":"CLOUD COMPUTING","descricao":"O cloud computing (computação em nuvem) refere-se à utilização da memória e da capacidade de armazenamento e cálculo de computadores e servidores compartilhados e interligados por meio da internet, seguindo o princípio da computação em grade. Saiba mais sobre cloud computing acessando nosso blog."},
        {"termo":"CLUSTER","descricao":"Um cluster consiste em computadores vagamente ou fortemente ligados que trabalham em conjunto para que, em muitos aspectos, eles possam ser vistos como um único sistema. Saiba mais sobre o funcionamento de um cluster acessando o post em nosso blog."},
        {"termo":"CMDB ou BDGC","descricao":"O CMBD (configuration management database – banco de dados do gerenciamento de configuração, ou BDGC) é um repositório de informações relacionadas a todos os itens de configuração na infraestrutura de TI. O CMDB é um componente fundamental do processo de gerenciamento de configuração do ITIL. O CMDB é a fonte confiável de informação sobre configuração de todo e qualquer componente do ambiente de TI, incluindo servidores, roteadores, desktops, impressoras, telefones, softwares, etc."},
        {"termo":"COBOL","descricao":"O COBOL (COmmon Business Oriented Language – linguagem comum orientada para negócios) é uma antiga linguagem de programação orientada para o processamento de banco de dados comerciais."},
        {"termo":"COBIT","descricao":"O COBIT (control objectives for information and related technology) auxilia a empresa a ter uma governança de TI mais controlada. É focado no negócio e tem por finalidade fornecer aos gestores um modelo de governança que ajude a entregar valor a TI e também a gerenciar de forma mais clara os riscos associados a área."},
        {"termo":"COOKIES","descricao":"Cookies são pequenos arquivos que ficam armazenados no computador. Eles são feitos para guardar dados específicos de um cliente ou website, para serem acessados futuramente pelo servidor web de maneira mais rápida."},
        {"termo":"CPU","descricao":"Central processor unit ou unidade central de processamento é o responsável pelo controle e execução das tarefas do computador, ou seja, é onde todas as operações do computador são processadas."},
        {"termo":"CRM","descricao":"O CRM (customer relationship management – gerenciamento do relacionamento com o cliente) é um sistema integrado de gestão com foco no cliente, que reune vários processos e tarefas de uma forma organizada e integrada."},
        {"termo":"DATACENTER (DC) ou CPD","descricao":"O datacenter (DC) ou CPD (centro de processamento de dados) é um ambiente projetado para abrigar servidores e outros componentes como sistemas de armazenamento de dados e ativos de rede."},
        {"termo":"DBMS ou SGBD","descricao":"DBMS é a sigla para data base management system, em português a sigla é SGBD (sistema de gerenciamento de banco de dados)."},
        {"termo":"DASHBOARDS","descricao":"Os dashboards são painéis visuais que centralizam informações importantes para o negócios. Permitem, desta forma, entender o cenário em tempo real e tomar decisões baseadas em informações reais e que estão ocorrendo agora, monitoradas minuto a minuto."},
        {"termo":"DDR","descricao":"O DDR (double data rate) é a taxa de transferência dobrada."},
        {"termo":"DEVOPS","descricao":"Derivada da junção entre Desenvolvedor e Operações, DevOps é uma metodologia de desenvolvimento de software que busca maximizar os resultados das equipes de TI. A metodologia integra toda a equipe de colaboradores e torna mais eficiente a comunicação entre eles, elevando o desenvolvimento de software e as equipes de TI a um novo patamar. As empresas que possuem as suas equipes de TI têm essas equipes compostas por profissionais especializados em diversas tecnologias, linguagens e operação em sistemas, que vão do RH ao financeiro, passando por todos os setores chaves da empresa."},
        {"termo":"DHTML","descricao":"O DHTML é uma união de tecnologias HTML, JavaScript e CSS, aliadas a um Modelo de Objeto de Documentos (DOM), que permitem a interatividade e animação de websites modificando a dinâmica na própria máquina, sem a necessidade de acessos a um servidor web."},
        {"termo":"DOWNTIME","descricao":"O downtime é o tempo que o sistema não está operacional. Saiba como calcular o custo de um downtime."},
        {"termo":"DNS","descricao":"DNS (domain name system – sitema de nomes e domínios) é um sistema que gerencia nomes e reconhece seus números de IP para fazer uma conexão. Devido ao DNS pode-se, por exemplo, digitar na barra de endereços do navegador ’www.opservices.com.br’ para acessar o site ao invés de um monte de números e pontos."},
        {"termo":"ELASTIC SEARCH","descricao":"O elasticsearch é uma ferramenta para buscas de código aberto (opensource) que tem capacidade para tratar de grandes quantidades de dados em tempo real. O servidor de buscas distribuído é baseado em Apache Lucene e desenvolvido em Java, utilizando uma interface comum, JSON sobre  "},
        {"termo":"ERP","descricao":"O ERP (enterprise resource planning – planejamento de recursos da empresa) é um sistema de gestão empresarial. Sua função é unificar os sistemas de diversas áreas da empresa, por exemplo, ao invés de cada área utilizar um sistema diferente de gerenciamento de suas atividades, uma solução de ERP é capaz de unificar estas informações, tornando os departamentos integrados."},
        {"termo":"ETHERNET","descricao":"É um protocolo de conexão para redes locais (LAN) com base no envio de pacotes."},
        {"termo":"FIREWALL","descricao":"O firewall é um dispositivo de uma rede de computadores que tem por objetivo aplicar uma política de segurança a um determinado ponto da rede."},
        {"termo":"FAQ","descricao":"FAQ (frequently asked questions) é a lista de perguntas e respostas mais freqüentes feitas pelos internautas."},
        {"termo":"FTE","descricao":"O FTE (full-time equivalent – equivalente a tempo completo) é um método de mensuração do grau de envolvimento de um colaborador nas atividades de uma organização ou unicamente em um determinado projeto."},
        {"termo":"FTP","descricao":"O FTP (file transfer protocol – protocolo de transferência de arquivos) é uma forma rápida e simples de transferência de arquivos na internet. Pode representar tanto o computador que transfere o arquivo quanto o servidor FTP."},
        {"termo":"GATEWAY","descricao":"Computador que interliga duas ou mais redes que usem protocolos de comunicação internos diferentes."},
        {"termo":"HOTSPOT","descricao":"Hotspot é o nome dado ao local onde a tecnologia wi-fi está disponível. São encontrados geralmente em locais públicos como cafés, restaurantes, hotéis e aeroportos onde é possível conectar-se à internet utilizando qualquer dispositivo que esteja preparado para se comunicar em uma rede sem fio do tipo wi-fi."},
        {"termo":"HTML","descricao":"O HMTL (hyper text markup language) é a linguagem de marcação mais popular na internet."},
        {"termo":"HUB","descricao":"O hub é a denominação dada ao equipamento para onde convergem dados que chegam de uma ou várias fontes e seguem para um ou vários destinos, dependendo do tipo e do comando recebido. Pode incluir uma switch e um roteador."},
        {"termo":"IAAS","descricao":"IaaS (infrastructure as a service – infraestrutura como serviço) é uma modalidade onde o cliente contrata a infraestrutura como serviço, ou seja, contrata servidores virtuais ao invés de servidores físicos."},
        {"termo":"INTRANET","descricao":"A intranet é a rede interna de computadores de uma empresa."},
        {"termo":"I&O","descricao":"O termo I&O (infrastructure & operations) designa os profissionais das áreas de infraestrutura e operações."},
        {"termo":"IOBVD","descricao":"IOBVD (infrastructure & operations business value dashboards). Diferentemente dos sistemas tradicionais utilizados pelos profissionais de TI e I&O, que se concentram apenas em exibir resultados operacionais, o IOBVD consegue cruzar essas informações com dados financeiros, comerciais, logísticos e de diferentes áreas e apresentar um resultado que indique, por exemplo, qual foi o impacto no faturamento gerado pela indisponibilidade de uma determinada aplicação. Ou, em uma visão orientada à vendas, a partir de uma queda nos resultados das vendas online descobrir se a causa-raíz deste problema está na diminuição da demanda ou é originada de um problema na operação ou de infraestrutura de TI. Saiba mais sobre IOBVD acessando nosso post."},
        {"termo":"IOT","descricao":"Internet of things (IOT) ou internet das coisas é o termo utilizado para a conexão de basicamente qualquer coisa à internet, seja ela um eletrodoméstico, dispositivo, tênis, enfim, qualquer coisa. Para saber mais sobre IOT acesse nosso post exclusivo sobre o assunto."},
        {"termo":"ITIL","descricao":"ITIL (information technology infrastructure library – biblioteca de infraestrutura de TI) é um conjunto de melhores práticas para gerenciar os serviços de TI."},
        {"termo":"IP e IPv6","descricao":"O IP é um endereço único que diferencia cada dispositivo conectado à internet. O IPv6 é a versão mais atualizada do IP, e possui mais números e protocolos. Surgiu para substituir gradativamente o IPv4, que já não suporta mais o cenário atual de IPs."},
        {"termo":"JAVA","descricao":"Java é uma linguagem de programação interpretada orientada a objetos. Diferente das linguagens de programação convencionais, que são compiladas para código nativo, é compilad para um bytecode (código de um programa escrito na linguagem Java) que é executado por uma máquina virtual (JVMs)."},
        {"termo":"JBOSS","descricao":"JBoss é um servidor de aplicação de código fonte aberto baseado na plataforma JEE (Java Enterprise Edition) e implementado completamente na linguagem de programação Java."},
        {"termo":"LAN","descricao":"A LAN (local area network – rede de área local) é uma rede local que tem por finalidade a troca de dados dentro um mesmo espaço físico. O limitador da rede LAN é uma faixa de IP restrita à mesma, com uma máscara de rede comum."},
        {"termo":"LAAS","descricao":"LaaS (license as a service – licença como serviço) é uma modalidade de comercialização onde o cliente adquire em definitivo as licenças de determinado software."},
        {"termo":"OCR","descricao":"O OCR (optical character recognition) é um programa utilizado para reconhecer textos existentes em documentos digitalizados, tornando a informação disponível para ser utilizada."},
        {"termo":"M2M","descricao":"O M2M (machine to machine – máquina à máquina) é a tecnologia que permite que qualquer objeto transmita dados através de um sensor. Esse sensor captura dados como geolocalização, temperatura, e outros, e os envia por meio de uma rede a um software."},
        {"termo":"MAINFRAME","descricao":"É um computador de grande porte, com foco no processamento de um grande volume de informações."},
        {"termo":"MARIADB","descricao":"O MariaDB é um banco de dados ramificado do MySQL. Após a aquisição do MySQL pela Oracle o seu desenvolvedor criou o MariaDB para manter a alta fidelidade ao MySQL."},
        {"termo":"MySQL","descricao":"O MySQL é um SGBD (sistema de gestão de banco de dados) que utiliza a linguagem SQL como interface."},
        {"termo":"MS SQL","descricao":"O Microsoft SQL Server é um SGDB desenvolvido pela Microsoft. Sua principal função é a de armazenar e recuperar dados solicitados por outras aplicações de software, sejam aqueles no mesmo computador ou aqueles em execução em outro computador através de uma rede (incluindo a Internet)."},
        {"termo":"MTTR e MTBF","descricao":"O MTTR (mean time to repair – tempo médio para reparo) é a média de tempo que se leva para executar um reparo após a ocorrência da falha. Ou seja, é o tempo gasto durante a intervenção em um determinado processo. O MTBF (mean time between failures – tempo médio entre falhas) é uma métrica que diz respeito à média de tempo decorrido entre uma falha e a próxima vez que ela ocorrerá. Esses lapsos de tempo podem ser calculados por meio de uma formula que você pode conferir acessando nosso post específico sobre o assunto."},
        {"termo":"NETFLOW","descricao":"A partir de sua instalação no roteador Cisco, o NetFlow passa a identificar os pacotes de dados não mais isoladamente, como outras tecnologias, mas como fluxos com início, meio e fim. Quando os fluxos são identificados, eles são armazenados no NetFlow Cache para caracterização e compreensão do tráfego da rede. Após 30 minutos são apagados da memória."},
        {"termo":"NFC","descricao":"A NFC foi criada por meio de um consórcio de gigantes da tecnologia: LG, Motorola, Samsung, Huawei, HTC, Google, Visa, Microsoft e Intel. Os celulares compatíveis com essa tecnologia possuem um chip na parte traseira do aparelho que estabelece a comunicação com outros aparelhos que possuem um chip equipado com a NFC. A NFC permite conexões rápidas em uma curta distância. Por isso, a transmissão de arquivos grandes não é ainda o objetivo dessa tecnologia. Essa tecnologia é baseada em uma já bastante difundida pelo mundo, a RFID (radio frequency identification – identificação por radiofrequência)."},
        {"termo":"ORACLE","descricao":"O Oracle é um SGBD (sistema de gestão de banco de dados) escrito em linguagem C e disponível em diversas plataformas materiais."},
        {"termo":"P2P","descricao":"O P2P (peer to peer – ponto a ponto ou par a par) é uma arquitetura de redes de computadores onde cada um dos pontos ou nós da rede funciona tanto como cliente quanto como servidor, permitindo compartilhamentos de serviços e dados sem a necessidade de um servidor central."},
        {"termo":"PDF","descricao":"PDF (portable document format) significa formato de documento portátil."},
        {"termo":"QUERY","descricao":"Query é o processo de extração de dados de um banco de dados e sua apresentação em uma forma adequada ao uso."},
        {"termo":"RACK","descricao":"Local físico onde se monta um conjunto de equipamentos relacionados, como servidores, roteadores, switches, etc."},
        {"termo":"ROI","descricao":"O ROI (return on investment – retorno sobre o investimento) é a mensuração do tempo necessário para retornar o valor investido na aquisição de novas soluções tecnológicas. Para aprender como calcular o ROI em projetos de TI acesse nosso post."},
        {"termo":"SAAS","descricao":"SaaS (software as a service – software como serviço) é uma forma de comercialização de software onde o fornecedor se responsabiliza por toda a estrutura necessária para a disponibilização do sistema."},
        {"termo":"SFLOW","descricao":"O sFlow é uma simplificação do protocolo NetFlow. Sendo também um protocolo e possui o conceito de Probe (agente projetado para coletar informações diretamente de uma rede) e Collector(servidor central que recolhe os datagramas de todos os agentes para armazenar e analisar)."},
        {"termo":"SCM","descricao":"O SCM (supply chain management – gerenciamento da cadeia de suprimentos) é uma solução que possibilita à empresa gerenciar a cadeia de suprimentos com maior eficácia e eficiência. Consiste basicamente em todas as partes relacionadas, direta ou indiretamente, na execução do pedido do cliente."},
        {"termo":"SQL","descricao":"SQL (structured query language – linguagem de consulta estruturada) é a linguagem de pesquisa declarativa padrão para banco de dados relacional."},
        {"termo":"SLA","descricao":"Fundamental para qualquer contrato de prestação de serviços na área de TI, o SLA (Service Level Agreement – acordo de nível de serviço, ou ANS) é a especificação, em termos mensuráveis e claros, de todos os serviços que o contratante pode esperar do contratado na relação contratual, bem como termos de compromisso, metas de nível de serviço, suporte técnico, prazos contratuais, dentre outros aspectos. Em outras palavras, é um esclarecimento técnico do contrato. É importante deixar claro que o SLA é um documento exigido em qualquer relação contratual de TI, sendo descrito na ABNT (associação brasileiras de normas técnicas) NBR ISO-IEC 20000-1, e que deve ser revisto periodicamente para que tenha maior efetividade. É apenas com a revisão feita continuamente que o contratante pode ter a garantia de que a empresa de TI oferecerá suporte em todas as etapas do processo que, evidentemente, requerem cuidados e serviços diferenciados."},
        {"termo":"STACK","descricao":"O stack ou empilhamento consiste em dois ou mais switches com o objetivo de gerenciar a rede através de um único endereço IP."},
        {"termo":"TCO","descricao":"O TCO (total cost of ownership – custo total da posse) é a estimativa financeira sobre os custos diretos e indiretos relacionados à aquisição de um software ou hardware, além dos custos envolvidos para mantê-los funcionando depois de adquirido."},
        {"termo":"TCP/IP","descricao":"O TCP/IP é um conjunto de protocolos de comunicação entre computadores em rede. Seu nome vem de dois protocolos: o TCP (transmission control protocol – protocolo de controle de transmissão) e o IP (internet protocol – protocolo de internet). O conjunto de protocolos pode ser visto como um modelo de camadas, onde cada camada é responsável por um grupo de tarefas, fornecendo um conjunto de serviços bem definidos para o protocolo da camada superior."},
        {"termo":"TI e TIC","descricao":"TI (tecnologia da informação) e TIC (tecnologia da informação e comunicação) correspondem a um conjunto de recursos tecnológicos integrados entre si. O TIC consiste em todos os meios técnicos usados para tratar a informação e auxiliar na comunicação, o que inclui hardware e software."},
        {"termo":"UX","descricao":"Também conhecido como user experience ou experiência do usuário, é a preocupação dos designers e desenvolvedores em criar aplicações pensadas na visão e utilização de seus usuários."},
        {"termo":"URL","descricao":"A URL (uniform resource locator – localizador padrão de recursos) refere-se ao endereço de rede no qual se encontra algum recurso informático, como por exemplo um arquivo de computador ou um dispositivo periférico."},
        {"termo":"VM","descricao":"A VM (virtual machine – máquina virtual) consiste em um software de ambiente computacional, que executa programas como um computador real, também chamado de processo de virtualização."},
        {"termo":"VPN","descricao":"VPN (virtual private network – rede privada virtual) é uma rede de comunicação privada. Por fornecerem autenticação e confidencialidade na transmissão de dados, além de protocolos criptografados por tunelamento, as VPNs tornam-se mais seguras e confiáveis nas comunicações."},
        {"termo":"XML","descricao":"O XML (extensible markup language) é uma linguagem de marcação capaz de descrever diversos tipos de dados. É um dos subtipos da SGML (standard generalized markup language) e sua principal finalidade é facilitar o compartilhamento de informações através da internet. Uma das características fundamentais do XML é possibilitar a criação de uma infraestrutura única para diversas linguagens, facilitando a definição de linguagens desconhecidas."},
        {"termo":"WAN","descricao":"A WAN (wide area network – rede de longa distância) é uma rede de computadores que cobre uma área extensa como uma universidade, cidade, estado ou até mesmo um país."},
        {"termo":"WEB","descricao":"WWW (world wide web) ou WEB é a sigla que denomina a rede mundial de computadores ligados em rede."},
        {"termo":"WEBNAR","descricao":"O webnar (ou webinar) é um tipo de conferência web no qual a comunicação é de uma via apenas, ou seja, somente uma pessoa se expressa e as outras assistem. A interação entre os participantes é limitada apenas ao chat, de modo que eles podem conversar entre si ou enviar perguntas ao palestrante."},
        {"termo":"WEBSPHERE","descricao":"WebSphere é o nome de uma família de softwares da IBM para criação e execução de aplicações baseadas no padrão Java J2EE, fornecendo também infraestrutura para integração de aplicações corporativas."}
 ]
    
var listaUl = document.querySelector("#busca ul");

for(var i = 0; i< dicionario.length; i++){
    var li = document.createElement("li");
    var h3 = document.createElement("h3");
    var span = document.createElement("span");

    h3.innerText = dicionario[i].termo;
    span.innerText = dicionario[i].descricao;

    li.appendChild(h3);

    li.appendChild(span);

    listaUl.appendChild(li);
}

var pesquisaInput = document.querySelector("#pesquisa");
pesquisaInput.onkeyup = function(){

    var lis = document.querySelectorAll("#busca li");
        for (var i = 0; i < lis.length; i++){   
            var termo = lis[i].querySelector("h3");
            lis[i].style.display = "block";
            if (termo.innerText.toLowerCase().search(this.value.toLowerCase()) > -1){
                console.log("existe");
            } 
            else {
                console.log("nao existe");  
                lis[i].style.display = "none";
            }
    }
}




//ADICIONANDO MARGEM DINAMICA PARA A DIV DEPOIS DO HEADER

$(document).ready(function () {
    $('#inicial').css('marginTop', $('#header').outerHeight(true) );
    console.log("header funcionou")
});

$(document).ready(function() {
	$('#fullpage').fullpage({
})});

$('#fullpage').fullpage({
  scrollOverflow: true,
  responsiveWidth: 950
});




